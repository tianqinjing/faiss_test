#!/usr/bin/env python
# encoding: utf-8
# @Time    : 2018/10/9
# @Author  : Sally
import json
from tornado.web import Application
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.log import gen_log
from tornado.web import RequestHandler
from arctern import ArcternManager
import numpy as np
from PIL import Image
from io import BytesIO
import requests
import base64


class FeatureHandler(RequestHandler):
    def __init__(self, application, request, **kwargs):
        self.init()
        super(FeatureHandler, self).__init__(application, request, **kwargs)

    def init(self):
        self.arctern_manager = ArcternManager()
        URL = "http://47.106.46.103:8890/licensing/"
        signature = self.arctern_manager.get_client_signature()
        signature = base64.b64encode(signature)
        print(signature)
        r = requests.post(URL, data={'key': signature})
        print('returned: ', r)
        js = r.json()
        print('returned: ', r.json())
        signature = js['signature']
        signature2 = base64.b64decode(signature)
        license = base64.b64decode(js['license'])
        verified = self.arctern_manager.verify_license(signature2, license)
        print("verifed: {}".format(verified))
        bload_fate = self.arctern_manager.set_face_feat_model("/app/face-det-0.9.0.bin")
        print("load face_date model {}".format(bload_fate))
        bsucc = self.arctern_manager.set_face_feat_model('/app/face-gluon-2.5.0.bin')
        print('load feature model: {}'.format(bsucc))


    def post(self):
        data = self.requests.body
        if data is None:
            gen_log.error(u"传输数据为空")
            self.write({"code": -1,
                        "message": u"传输数据为空"})
            self.finish()
            return

        result = {
            "code": 0,
            "message": "ok"
        }
        image_data = base64.b64decode(data)
        buffers = BytesIO()
        buffers.write(image_data)
        buffers.seek(0)
        pil_image = Image.open(buffers)
        image = np.asarray(pil_image)
        faces = self.arctern_manager.detect_face(image)
        print(faces)
        if len(faces) > 0:
            faces = faces[0][0]
            feat = self.arctern_manager.get_face_feat(image, faces)
            print(feat)
            result["feat"] = json.dumps(feat)
            self.write(result)
            self.finish()
        else:
            self.write(result)
            self.finish()
            return


class MyApplication(Application):
    def __init__(self):
        handlers = [
            (r'/feature', FeatureHandler),
        ]
        settings = {
            'autoescape': None,
            'debug': True,
        }
        Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    import sys
    import traceback

    try:
        host = "0.0.0.0"
        port = 9000
        http_server = HTTPServer(
            MyApplication(),
            xheaders=True,
            idle_connection_timeout=10,
            body_timeout=10)
        http_server.listen(port, host)
        gen_log.info("Running on  host: %s, port: %d"%(
            host,
            port
        ))
        IOLoop.instance().start()
    except Exception as ex:
        print(ex)
        print(traceback.format_exc())
        sys.exit(1)